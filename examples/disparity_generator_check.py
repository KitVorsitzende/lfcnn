"""
Check if a generator works as intended.
Loads a dataset, create batches plot light field and labels.
"""
import numpy as np
import matplotlib.pyplot as plt
from itertools import product
from pathlib import Path

from lfcnn.generators import CentralAndDisparityGenerator
from lfcnn.generators.reshapes import lf_identity

# Optionally, set CPU as device:
from lfcnn.utils import tf_utils
tf_utils.use_cpu()

data_path = data_base_path / "train.h5"
valid_path = data_base_path / "validation.h5"
test_path = data_base_path / "test.h5"
data_range = 2**16 - 1

augment = dict(flip=True,
               rotate=True,
               weigh_chs=True,
               gamma=True,
               permute_chs=True,
               scale=True)
batchsize = 3
num_batches = 1
kwargs = dict(data=test_path,
              data_key="lf",
              label_keys="disp",
              augmented_shape=(7, 7, 32, 32, 3),    # Angular and spatial crop
              generated_shape=(7, 7, 32, 32, 3),  # Identity, sanity check
              reshape_func=lf_identity,
              batch_size=batchsize,
              range_data=data_range,
              data_percentage=1,
              augment=augment,
              shuffle=True,
              use_mask=False,
              fix_seed=False)

# Init Generator
gen = CentralAndDisparityGenerator(**kwargs)


# Plot
fig, ax = plt.subplots(3 * num_batches, batchsize, sharex=True, sharey=True)

for i in range(num_batches):
    lf, labels = gen.__getitem__(i+150)

    disp = labels["disparity"]
    central = labels["central_view"]

    for j in range(batchsize):
            ax[3 * i, j].imshow(np.squeeze(lf[j, 3, 3, :, :, :3]))
            ax[3 * i + 1, j].imshow(np.squeeze(central[j, :, :, :3]))
            ax[3 * i + 2, j].imshow(np.squeeze(disp[j]))

for i, j in product(range(3*num_batches), range(batchsize)):
    ax[i,j].set_axis_off()

plt.tight_layout(pad=0.1)
plt.show()
