"""A script to split a full initial dataset into training, validation and
testing datasets including a shuffling of the data.
"""

from pathlib import Path

import h5py
import numpy as np

###############################################################################
# SCRIPT SETUP
###############################################################################

# Set path to patched light field Initialset.h5
dataset_path = Path('<path-to-save-folder>')

initial_dataset_path = dataset_path / 'Initialset.h5'
training_dataset_path = dataset_path / 'Trainingset.h5'
validation_dataset_path = dataset_path / 'Validationset.h5'
testing_dataset_path = dataset_path / 'Testingset.h5'

# If one of the target files exists, overwrite?
overwrite = True

# Set dataset split amounts
training_dataset_percentage = 0.9
validation_dataset_percentage = 0.05
testing_dataset_percentage = 0.05


###############################################################################
# SCRIPT START
###############################################################################

if not training_dataset_percentage + validation_dataset_percentage + testing_dataset_percentage == 1:
    raise ValueError("Data split percentages do not add up to 1.")

if training_dataset_path.exists() and not overwrite:
    raise FileExistsError(f"Datasetfile {training_dataset_path} already exists. "
                          f"Either delete, rename or specify overwrite = True.")

if validation_dataset_path.exists() and not overwrite:
    raise FileExistsError(f"Datasetfile {validation_dataset_path} already exists. "
                          f"Either delete, rename or specify overwrite = True.")

if testing_dataset_path.exists() and not overwrite:
    raise FileExistsError(f"Datasetfile {testing_dataset_path} already exists. "
                          f"Either delete, rename or specify overwrite = True.")


# Create dataset folder if it does not exist
dataset_path.mkdir(exist_ok=True)

train_p = int(training_dataset_percentage*100)
valid_p = int(validation_dataset_percentage*100)
test_p = int(testing_dataset_percentage*100)

print(f"The dataset will be split {train_p}:{valid_p}:{test_p} into training, validation and test datasets.")

with h5py.File(initial_dataset_path, "r") as fh_init:
    # Get input data and input shape
    data_init = fh_init['lf']
    label_init = fh_init['disp']
    num_patches, u, v, s, t, num_ch = fh_init['lf'].shape

    # Create shuffled list of patch indices
    patches = np.arange(num_patches)
    np.random.shuffle(patches)
    patches = patches.tolist()

    # Calculate size of split datasets
    num_train = int(training_dataset_percentage * num_patches)
    num_valid = int(testing_dataset_percentage * num_patches)
    num_test = num_patches - num_train - num_valid

    # Extract dataset patches
    a = num_train
    b = a + num_valid
    c = b + num_test
    patches_train = patches[:a]
    patches_valid = patches[a:b]
    patches_test = patches[b:c]

    # Safety check
    if not set(patches_train + patches_valid + patches_test) == set(patches):
        raise ValueError("The sets do not add up. Something went wrong.")

    tmp_paths = [training_dataset_path, validation_dataset_path, testing_dataset_path]
    tmp_patches = [patches_train, patches_valid, patches_test]

    # Iterate over train, validation and test
    for tmp_path, tmp_patches in zip(tmp_paths, tmp_patches):
        tmp_num_patches = len(tmp_patches)

        print(f"Creating {tmp_path} dataset containing {tmp_num_patches} patches.")
        with h5py.File(tmp_path, 'w') as fh_tmp:
            # Init new dataset
            tmp_data = fh_tmp.create_dataset(
                'lf', (tmp_num_patches, u, v, s, t, num_ch), dtype=np.uint16)
            tmp_label = fh_tmp.create_dataset(
                'disp', (tmp_num_patches, s, t), dtype=np.float32)

            for i, idx in enumerate(tmp_patches):
                print(f"Processing {i+1} of {len(tmp_patches)}.")
                # i: linear index, idx: shuffled index
                tmp_data[i] = data_init[idx]
                tmp_label[i] = label_init[idx]

print('INFO: Done.')
