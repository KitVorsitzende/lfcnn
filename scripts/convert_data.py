"""A script to convert a set of light fields and disparities into
a full initial set in the h5 format used by default with lfcnn.
The full dataset can then be split using the provided
shuffle_and_split_data.py script.
Note that here only the central disparity view is saved as a label.

You may need to install the plenpy library to handle light fields.
See: https://gitlab.com/iiit-public/plenpy
"""

from pathlib import Path
from itertools import product

import h5py
import numpy as np
from plenpy.lightfields import LightField

###############################################################################
# SCRIPT SETUP
###############################################################################

# Set path to original light field Data
data_path = Path('<path-to-folder-containing-lf>')

# Datest Setup, paths and distribution
dataset_path = Path('<path-to-save-folder>')
initial_dataset_path = dataset_path / 'Initialset.h5'

# If Initialset.h5 already exists, overwrite?
overwrite = True

# Format of input light fields (number of channels is unchanged or converted to RGB)
input_shape = (11, 11, 512, 512, 13)
u, v, s, t, num_ch = input_shape
border = 4

# Format of cropped light fields
# Should have odd angular resolution such that a well-defined central view exists
output_shape = (9, 9, 36, 36)
(u_c, v_c, s_c, t_c) = output_shape

# Whether to convert multispectral into RGB light fields
conv_to_RGB = False

###############################################################################
# SCRIPT START
###############################################################################

if not all(x <= y for x, y in zip(output_shape, input_shape)):
    raise ValueError(f"Incompatible output shape {output_shape} "
                     f"and input shape {input_shape}.")

if u % 2 == 0 or v % 2 == 0:
    print("WARNING: Input angular shape is even. "
          "No well-defined central view exists. "
          "This script might not behave as expected.")

if u_c % 2 == 0 or v_c % 2 == 0:
    print("WARNING: Cropped angular shape is even. "
          "No well-defined central view exists. "
          "This script might not behave as expected.")

if not conv_to_RGB:
    num_ch_c = num_ch
else:
    num_ch_c = 3

# Create dataset folder if it does not exist
dataset_path.mkdir(exist_ok=True)
if initial_dataset_path.exists() and not overwrite:
    raise FileExistsError(f"Datasetfile {initial_dataset_path} already exists. "
                          f"Either delete, rename or specify overwrite = True.")



# Calculate number of light field patches per light field
s_range, t_range = int((s - 2*border) / s_c), int((t - 2*border) / t_c)
num_patches_per_lf = s_range * t_range
patch_loss_per_lf = s * t - s_range * s_c * t_range * t_c

if conv_to_RGB:
    print(f"Converting Spectrum to RGB.")

# Get all the light fields contained in the path.
lf_files = [x for x in data_path.glob('*.img')]

# Number of files found
num_lf_files = len(lf_files)
num_patches = num_lf_files * num_patches_per_lf

# Print information to data usage
print(f"Patching input light fields of shape {input_shape} to {output_shape} with a border of {border} px.")
print(f"{num_patches_per_lf} light field patches will be generated per single light field.")
print(f"There are {num_lf_files} files in the given path.")
print(f"{num_patches} patches will be created in total.")

# Set the central view for the disparity map (u, v odd)
u_center = u // 2
v_center = v // 2

# Set u, v offset when angular crop is needed
off_u = (u - u_c) // 2
off_v = (v - v_c) // 2
u_min, u_max = off_u, off_u + u_c
v_min, v_max = off_v, off_v + v_c

# Counter for the scenes
patch_counter = 0

# Open the dataset_path and create the initial full dataset
with h5py.File(initial_dataset_path, 'w') as d:
    # Init data, save as uint16 since raytraced data was uint16
    data = d.create_dataset('lf', (num_patches, u_c, v_c, s_c, t_c, num_ch_c),
                            dtype=np.uint16)
    # Init label, disparity was traced as float32
    label = d.create_dataset('disp', (num_patches, s_c, t_c),
                             dtype=np.float32)

    # Load each the Training Data and Prepare it
    counter = 0
    for i, file in enumerate(lf_files):
        # Print status
        print(f"Processing light field {i+1} of {len(lf_files)}.")

        # Load the multispectral light field
        lightfield = LightField.from_file(file, s_max=s, t_max=t, format='envi')

        # Convert to RGB if needed
        if conv_to_RGB:
            lightfield = lightfield.get_rgb()

        # Convert the light field to uint16 ndarray
        lightfield = ((2**16 - 1) * np.asarray(lightfield)).astype(np.uint16)

        # Get the paths to the leightfield an the disparity
        disp_path = file.with_name(file.stem + "_disp.pfm")

        # Load the disparity (here: only central view)
        # Important: flip disparity upside down due to pfm format
        disp_arr = np.asarray(LightField.from_file(disp_path, 512), dtype=np.float32)
        disp_arr = np.flip(disp_arr, (0, 2))
        disp = np.squeeze(disp_arr[u_center, v_center])

        # Create patches from current light field
        for s_iter, t_iter in product(range(s_range), range(t_range)):

            # Calculate start and end valeus
            s_min = border + s_c * s_iter
            s_max = border + s_c * (s_iter + 1)

            # Calculate start and end valeus
            t_min = border + t_c * t_iter
            t_max = border + t_c * (t_iter + 1)

            # Extract light field patch
            data[counter] = lightfield[u_min:u_max, v_min:v_max,
                            s_min:s_max, t_min:t_max, :]

            # Get and set disparity section
            label[counter] = disp[s_min:s_max, t_min:t_max]

            counter += 1

# Done
print('The data was patched.')
